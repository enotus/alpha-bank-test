//
//  WhiteCardHeader.swift
//  Alpha Bank Test
//
//  Created by Igor Volkov on 14/05/2018.
//  Copyright © 2018 enotus. All rights reserved.
//

import UIKit

@IBDesignable
class WhiteCardHeader: UIView {

    private func setupStyle() {
        backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9764705882, blue: 0.9803921569, alpha: 1)
        layer.cornerRadius = 16.0
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupStyle()
    }

}
