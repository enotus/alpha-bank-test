//
//  RedBigButton.swift
//  Alpha Bank Test
//
//  Created by Igor Volkov on 14/05/2018.
//  Copyright © 2018 enotus. All rights reserved.
//

import UIKit

@IBDesignable

class RedBigButton: UIButton {

//    let scaleFactor:CGFloat = 0.99
//
//    override var isHighlighted: Bool {
//        didSet {
//            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
//                self.alpha = self.isHighlighted ? 0.8 : 1.0
//                self.transform = self.isHighlighted ? CGAffineTransform(scaleX: self.scaleFactor, y: self.scaleFactor) : CGAffineTransform.identity
//            }, completion: nil)
//        }
//    }
    
    private func setupStyle() {
        backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2078431373, blue: 0.137254902, alpha: 1)
        layer.cornerRadius = 8.0
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupStyle()
    }

}
