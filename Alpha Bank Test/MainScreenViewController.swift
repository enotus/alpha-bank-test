//
//  ViewController.swift
//  Alpha Bank Test
//
//  Created by Enotus on 11/05/2018.
//  Copyright © 2018 enotus. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var amountControls: UIStackView!
    @IBOutlet weak var amountTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        amountControls.isHidden = true
        amountControls.alpha = 0.0
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func amountBeginEditing(_ sender: Any) {
        UIView.animate(withDuration: 0.18, delay: 0, options: [.curveEaseOut], animations: {
                self.amountControls.isHidden = false
                self.amountControls.alpha = 1.0
        }, completion: nil)
    }
    
    @IBAction func amountEndEditing(_ sender: Any) {
        UIView.animate(withDuration: 0.18, delay: 0, options: [.curveEaseOut], animations: {
            self.amountControls.isHidden = true
            self.amountControls.alpha = 0.0
        }, completion: nil)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        
        if !amountControls.isHidden {
            UIView.animate(withDuration: 0.18, delay: 0, options: [.curveEaseOut], animations: {
                self.amountControls.isHidden = true
                self.amountControls.alpha = 0.0
            }, completion: nil)
        }
    }
    
    @IBAction func changeAccount(_ sender: Any) {
        let alert = UIAlertController(title: "Выберите счёт", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Мой сейф ····5567, 103 203 ₽", style: .default , handler: nil ))
        alert.addAction(UIAlertAction(title: "Сбережения ····7867, 503 203 ₽", style: .default , handler: nil ))
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil ))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension MainScreenViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(MainScreenViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
