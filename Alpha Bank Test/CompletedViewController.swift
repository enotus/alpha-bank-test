//
//  CompletedViewController.swift
//  Alpha Bank Test
//
//  Created by Enotus on 14/05/2018.
//  Copyright © 2018 enotus. All rights reserved.
//

import UIKit

class CompletedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
