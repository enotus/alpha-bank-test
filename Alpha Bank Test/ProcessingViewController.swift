//
//  ProcessingViewController.swift
//  Alpha Bank Test
//
//  Created by Igor Volkov on 14/05/2018.
//  Copyright © 2018 enotus. All rights reserved.
//

import UIKit

class ProcessingViewController: UIViewController {

    @IBOutlet weak var cardTopPosition: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var spinner: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.alpha = 0.0
        rotate(view: spinner)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        processingAnimation()
        
    }

    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func rotate(view: UIView, duration: Double = 0.4) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            view.transform = view.transform.rotated(by: CGFloat.pi)
        }) { finished in
            self.rotate(view: view, duration: duration)
        }
    }
    
    func processingAnimation() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: {
            self.cardTopPosition.constant = 164
            self.cancelButton.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: { _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Completed")
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
        })
    }
}
