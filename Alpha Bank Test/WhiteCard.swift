//
//  WhiteCard.swift
//  Alpha Bank Test
//
//  Created by Igor Volkov on 14/05/2018.
//  Copyright © 2018 enotus. All rights reserved.
//

import UIKit

@IBDesignable
class WhiteCard: UIView {
    
    private func setupStyle() {
        backgroundColor = UIColor.white
        layer.cornerRadius = 16.0
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 8)
        layer.shadowRadius = 28.0
        layer.shadowOpacity = 0.48
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupStyle()
    }

}
